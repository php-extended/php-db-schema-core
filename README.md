# php-extended/php-db-schema-core
A library to provide all the core objects to help vendor specific packages

![coverage](https://gitlab.com/php-extended/php-db-schema-core/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-db-schema-core/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-db-schema-core ^9`


## License

MIT (See [license file](LICENSE)).
