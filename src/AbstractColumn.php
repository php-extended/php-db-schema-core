<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * AbstractColumn class file.
 *
 * This class is a base implementation of the ColumnInterface.
 *
 * @author Anastaszor
 */
abstract class AbstractColumn implements ColumnInterface
{
	
	/**
	 * The name of the column.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Whether this column allows null values.
	 *
	 * @var boolean
	 */
	protected bool $_allowsNull = false;
	
	/**
	 * The comment of this column.
	 *
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * Builds a new AbstractColumn with its name, type, ability to be null and
	 * comment.
	 * 
	 * @param string $name
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, bool $allowsNull = false, ?string $comment = null)
	{
		$this->_name = \trim($name);
		$this->_allowsNull = $allowsNull;
		$this->_comment = $comment;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::isNullAllowed()
	 */
	public function isNullAllowed() : bool
	{
		return $this->_allowsNull;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::getComment()
	 */
	public function getComment() : string
	{
		return (string) $this->_comment;
	}
	
}
