<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueSpatial class file.
 * 
 * This class is a simple implementation of the StatementFixedValueSpatialInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueSpatial extends AbstractStatementFixedValue implements StatementFixedValueSpatialInterface
{
	
	/**
	 * The type of value.
	 * 
	 * @var TypeSpatialInterface
	 */
	protected TypeSpatialInterface $_type;
	
	/**
	 * Builds a new StatementFixedValueSpatial with the given value and type.
	 * 
	 * @param string $value
	 * @param TypeSpatialInterface $type
	 */
	public function __construct(string $value, TypeSpatialInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueSpatialInterface::getType()
	 */
	public function getType() : TypeSpatialInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueSpatial($this);
	}
	
}
