<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallSpatial class file.
 * 
 * This class represents a simple implementation of the StatementFunctionCallSpatialInterface.
 * 
 * @author Anastaszor
 */
class StatementFunctionCallSpatial extends AbstractStatementFunctionCall implements StatementFunctionCallSpatialInterface
{
	
	/**
	 * The return type of this function.
	 * 
	 * @var TypeSpatialInterface
	 */
	protected TypeSpatialInterface $_type;
	
	/**
	 * Builds a new StatementFunctionCallSpatial with its name, return type
	 * and argument types.
	 * 
	 * @param string $name
	 * @param TypeSpatialInterface $return
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, TypeSpatialInterface $return, array $parameters)
	{
		parent::__construct($name, $parameters);
		$this->_type = $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueSpatialInterface::getType()
	 */
	public function getType() : TypeSpatialInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFunctionCallSpatial($this);
	}
	
}
