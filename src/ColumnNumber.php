<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnNumber class file.
 *
 * This class is a base implementation of the ColumnNumberInterface.
 *
 * @author Anastaszor
 */
class ColumnNumber extends AbstractColumn implements ColumnNumberInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeNumberInterface
	 */
	protected TypeNumberInterface $_type;
	
	/**
	 * The choosen length for the type.
	 *
	 * @var integer
	 */
	protected int $_length;
	
	/**
	 * The choosen precision for the type.
	 *
	 * @var ?integer
	 */
	protected ?int $_precision = null;
	
	/**
	 * Whether this column has an unsigned value.
	 *
	 * @var boolean
	 */
	protected bool $_isUnsigned = false;
	
	/**
	 * Whether this column has an auto_increment value. (Works only for
	 * integer typed columns).
	 *
	 * @var boolean
	 */
	protected bool $_hasAutoIncrement = false;
	
	/**
	 * Builds a new ColumnNumber with the given number type.
	 * 
	 * @param string $name
	 * @param TypeNumberInterface $type
	 * @param integer $length
	 * @param integer $precision
	 * @param boolean $unsigned
	 * @param boolean $autoIncrement
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeNumberInterface $type, int $length, ?int $precision = null, bool $unsigned = false, bool $autoIncrement = false, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
		$this->_length = $length;
		$this->_precision = $precision;
		$this->_isUnsigned = $unsigned;
		$this->_hasAutoIncrement = $autoIncrement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnNumber($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnNumberInterface::getType()
	 */
	public function getType() : TypeNumberInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnNumberInterface::getLength()
	 */
	public function getLength() : int
	{
		return $this->_length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnNumberInterface::getPrecision()
	 */
	public function getPrecision() : ?int
	{
		return $this->_precision;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnNumberInterface::isUnsigned()
	 */
	public function isUnsigned() : bool
	{
		return $this->_isUnsigned;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnNumberInterface::hasAutoIncrement()
	 */
	public function hasAutoIncrement() : bool
	{
		return $this->_hasAutoIncrement;
	}
	
}
