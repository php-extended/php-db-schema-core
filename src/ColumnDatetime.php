<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnDatetime class file.
 *
 * This class is a base implementation of the ColumnDatetimeInterface.
 *
 * @author Anastaszor
 */
class ColumnDatetime extends AbstractColumn implements ColumnDatetimeInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeDatetimeInterface
	 */
	protected TypeDatetimeInterface $_type;
	
	/**
	 * True if the column should be auto incremented.
	 *
	 * @var boolean
	 */
	protected bool $_autoupdate = false;
	
	/**
	 * Builds a new ColumnDatetime with the given date time type.
	 * 
	 * @param string $name
	 * @param TypeDatetimeInterface $type
	 * @param boolean $autoupdate
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeDatetimeInterface $type, bool $autoupdate = false, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
		$this->_autoupdate = $autoupdate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnDatetime($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnDatetimeInterface::hasAutoupdate()
	 */
	public function hasAutoupdate() : bool
	{
		return $this->_autoupdate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractColumn::getType()
	 */
	public function getType() : TypeDatetimeInterface
	{
		return $this->_type;
	}
	
}
