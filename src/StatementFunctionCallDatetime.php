<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallDatetime class file.
 * 
 * This class represents a simple implementation of the StatementFunctionCallDatetimeInterface.
 * 
 * @author Anastaszor
 */
class StatementFunctionCallDatetime extends AbstractStatementFunctionCall implements StatementFunctionCallDatetimeInterface
{
	
	/**
	 * The return type of this function call.
	 * 
	 * @var TypeDatetimeInterface
	 */
	protected TypeDatetimeInterface $_type;
	
	/**
	 * Builds a new StatementFunctionCallDatetime with its name, return type 
	 * and argument types.
	 * 
	 * @param string $name
	 * @param TypeDatetimeInterface $return
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, TypeDatetimeInterface $return, array $parameters)
	{
		parent::__construct($name, $parameters);
		$this->_type = $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueDatetimeInterface::getType()
	 */
	public function getType() : TypeDatetimeInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFunctionCallDatetime($this);
	}
	
}
