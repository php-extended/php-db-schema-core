<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueNumber class file.
 * 
 * This class is a simple implementation of the StatementFixedValueNumberInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueNumber extends AbstractStatementFixedValue implements StatementFixedValueNumberInterface
{
	
	/**
	 * The type of the value.
	 * 
	 * @var TypeNumberInterface
	 */
	protected TypeNumberInterface $_type;
	
	/**
	 * Builds a new StatementFixedValueNumber with the given value and type.
	 * 
	 * @param string $value
	 * @param TypeNumberInterface $type
	 */
	public function __construct(string $value, TypeNumberInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueNumberInterface::getType()
	 */
	public function getType() : TypeNumberInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueNumber($this);
	}
	
}
