<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnString class file.
 * 
 * This class is a simple implementation of the StatementColumnStringInterface.
 * 
 * @author Anastaszor
 */
class StatementColumnString implements StatementColumnStringInterface
{
	
	/**
	 * The access modifier.
	 * 
	 * @var StatementAccessModifierInterface
	 */
	protected StatementAccessModifierInterface $_accessModifier;
	
	/**
	 * The datetime column.
	 * 
	 * @var ColumnStringInterface
	 */
	protected ColumnStringInterface $_column;
	
	/**
	 * Builds a new StatementColumnString with the given datetime
	 * column.
	 * 
	 * @param StatementAccessModifierInterface $modifier
	 * @param ColumnStringInterface $column
	 */
	public function __construct(StatementAccessModifierInterface $modifier, ColumnStringInterface $column)
	{
		$this->_accessModifier = $modifier;
		$this->_column = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnInterface::getAccessModifier()
	 */
	public function getAccessModifier() : StatementAccessModifierInterface
	{
		return $this->_accessModifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnStringInterface::getColumn()
	 */
	public function getColumn() : ColumnStringInterface
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueStringInterface::getType()
	 */
	public function getType() : TypeStringInterface
	{
		return $this->getColumn()->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitColumnString($this);
	}
	
}
