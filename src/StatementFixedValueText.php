<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueText class file.
 * 
 * This class is a simple implementation of the StatementFixedValueTextInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueText extends AbstractStatementFixedValue implements StatementFixedValueTextInterface
{
	
	/**
	 * The type of the value.
	 * 
	 * @var TypeTextInterface
	 */
	protected TypeTextInterface $_type;
	
	/**
	 * Builds a new.
	 * 
	 * @param string $value
	 * @param TypeTextInterface $type
	 */
	public function __construct(string $value, TypeTextInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueTextInterface::getType()
	 */
	public function getType() : TypeTextInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueText($this);
	}
	
}
