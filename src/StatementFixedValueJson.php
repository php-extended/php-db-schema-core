<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueJson class file.
 * 
 * This class is a simple implementation of the StatementFixedValueJsonInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueJson extends AbstractStatementFixedValue implements StatementFixedValueJsonInterface
{
	
	/**
	 * The type of the value.
	 * 
	 * @var TypeJsonInterface
	 */
	protected TypeJsonInterface $_type;
	
	/**
	 * Builds a new StatementFixedValueJson with the given value and type.
	 * 
	 * @param string $value
	 * @param TypeJsonInterface $type
	 */
	public function __construct(string $value, TypeJsonInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueJsonInterface::getType()
	 */
	public function getType() : TypeJsonInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueJson($this);
	}
	
}
