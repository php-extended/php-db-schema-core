<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * ForeignKey class file.
 * 
 * This class is a simple implementation of the ForeignKeyInterface.
 * 
 * @author Anastaszor
 */
class ForeignKey implements ForeignKeyInterface
{
	
	/**
	 * The name of the relation.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The name of the target table.
	 * 
	 * @var string
	 */
	protected string $_targetTableName;
	
	/**
	 * The relations between fields of the source and the target table.
	 * 
	 * @var array<integer, ForeignKeyRelationInterface>
	 */
	protected array $_relations = [];
	
	/**
	 * The action on update of this key.
	 * 
	 * @var ForeignKeyActionInterface
	 */
	protected ForeignKeyActionInterface $_onUpdateAction;
	
	/**
	 * The action on delete of this key.
	 * 
	 * @var ForeignKeyActionInterface
	 */
	protected ForeignKeyActionInterface $_onDeleteAction;
	
	/**
	 * Builds a new ForeignKey with the given name, target class and the first
	 * relation between the source field and the target field.
	 * 
	 * @param string $name
	 * @param string $targetTableName
	 * @param ForeignKeyRelationInterface $relation
	 * @param ForeignKeyActionInterface $onUpdate
	 * @param ForeignKeyActionInterface $onDelete
	 */
	public function __construct(
		string $name,
		string $targetTableName,
		ForeignKeyRelationInterface $relation,
		ForeignKeyActionInterface $onUpdate,
		ForeignKeyActionInterface $onDelete
	) {
		$this->_name = \trim($name);
		$this->_targetTableName = \trim($targetTableName);
		$this->_relations[] = $relation;
		$this->_onUpdateAction = $onUpdate;
		$this->_onDeleteAction = $onDelete;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a relation to this foreign key.
	 * 
	 * @param ForeignKeyRelationInterface $relation
	 */
	public function addRelation(ForeignKeyRelationInterface $relation) : void
	{
		$this->_relations[] = $relation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyInterface::getTargetTableName()
	 */
	public function getTargetTableName() : string
	{
		return $this->_targetTableName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyInterface::getForeignKeyRelations()
	 * @return Iterator<ForeignKeyRelationInterface>
	 */
	public function getForeignKeyRelations() : Iterator
	{
		return new ArrayIterator($this->_relations);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyInterface::getOnUpdateAction()
	 */
	public function getOnUpdateAction() : ForeignKeyActionInterface
	{
		return $this->_onUpdateAction;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyInterface::getOnDeleteAction()
	 */
	public function getOnDeleteAction() : ForeignKeyActionInterface
	{
		return $this->_onDeleteAction;
	}
	
}
