<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallText class file.
 * 
 * This class represents a simple implementation of the StatementFunctionCallTextInterface.
 * 
 * @author Anastaszor
 */
class StatementFunctionCallText extends AbstractStatementFunctionCall implements StatementFunctionCallTextInterface
{
	
	/**
	 * The return type of this function call.
	 * 
	 * @var TypeTextInterface
	 */
	protected TypeTextInterface $_type;
	
	/**
	 * Builds a new StatementFunctionCallText with its name, return type
	 * and argument types.
	 * 
	 * @param string $name
	 * @param TypeTextInterface $return
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, TypeTextInterface $return, array $parameters)
	{
		parent::__construct($name, $parameters);
		$this->_type = $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueTextInterface::getType()
	 * @return TypeTextInterface
	 */
	public function getType() : TypeTextInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFunctionCallText($this);
	}
	
}
