<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableSpatial class file.
 * 
 * This class is a simple implementation of the StatementVariableSpatialInterface.
 * 
 * @author Anastaszor
 */
class StatementVariableSpatial extends AbstractStatementVariable implements StatementVariableSpatialInterface
{
	
	/**
	 * The type of variable.
	 * 
	 * @var TypeSpatialInterface
	 */
	protected TypeSpatialInterface $_type;
	
	/**
	 * Builds a new StatementVariableSpatial with the given name and type.
	 * 
	 * @param string $name
	 * @param TypeSpatialInterface $type
	 */
	public function __construct(string $name, TypeSpatialInterface $type)
	{
		parent::__construct($name);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueSpatialInterface::getType()
	 */
	public function getType() : TypeSpatialInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitVariableSpatial($this);
	}
	
}
