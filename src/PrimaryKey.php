<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * PrimaryKey class file.
 *
 * This class is a base implementation of the PrimaryKeyInterface.
 *
 * @author Anastaszor
 */
class PrimaryKey implements PrimaryKeyInterface
{
	
	/**
	 * The names of the columns of this primary key.
	 *
	 * @var array<integer, string>
	 */
	protected array $_columnNames = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a column to this primary key.
	 *
	 * @param string $columnName
	 */
	public function addColumn(string $columnName) : void
	{
		$this->_columnNames[] = \trim($columnName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\PrimaryKeyInterface::getColumnNames()
	 */
	public function getColumnNames() : array
	{
		return $this->_columnNames;
	}
	
}
