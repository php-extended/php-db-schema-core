<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueDatetime class file.
 * 
 * This class is a simple implementation of the StatementFixedValueDatetimeInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueDatetime extends AbstractStatementFixedValue implements StatementFixedValueDatetimeInterface
{
	
	/**
	 * The type of the value.
	 * 
	 * @var TypeDatetimeInterface
	 */
	protected TypeDatetimeInterface $_type;
	
	/**
	 * Builds a new StatementFixedValueDatetime with the given value and type.
	 * 
	 * @param string $value
	 * @param TypeDatetimeInterface $type
	 */
	public function __construct(string $value, TypeDatetimeInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueDatetimeInterface::getType()
	 */
	public function getType() : TypeDatetimeInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueDatetime($this);
	}
	
}
