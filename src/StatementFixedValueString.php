<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFixedValueString class file.
 * 
 * This class is a simple implementation of the StatementFixedValueStringInterface.
 * 
 * @author Anastaszor
 */
class StatementFixedValueString extends AbstractStatementFixedValue implements StatementFixedValueStringInterface
{
	
	/**
	 * The type of the value.
	 * 
	 * @var TypeStringInterface
	 */
	protected TypeStringInterface $_type;
	
	/**
	 * Builds a new StatementFixedValueString with the given value and type.
	 * 
	 * @param string $value
	 * @param TypeStringInterface $type
	 */
	public function __construct(string $value, TypeStringInterface $type)
	{
		parent::__construct($value);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueStringInterface::getType()
	 */
	public function getType() : TypeStringInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFixedValueString($this);
	}
	
}
