<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * Table class file.
 *
 * This class is a base implementation for the TableInterface.
 *
 * @author Anastaszor
 */
class Table implements TableInterface
{
	
	/**
	 * The name of the table.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The columns of the table.
	 *
	 * @var array<string, ColumnInterface>
	 */
	protected array $_columns = [];
	
	/**
	 * The primary key of the table.
	 *
	 * @var PrimaryKey
	 */
	protected PrimaryKey $_primaryKey;
	
	/**
	 * The foreign keys of the table.
	 * 
	 * @var array<string, ForeignKeyInterface>
	 */
	protected array $_foreignKeys = [];
	
	/**
	 * The indexes of the table.
	 *
	 * @var array<string, IndexInterface>
	 */
	protected array $_indexes = [];
	
	/**
	 * The triggers of the table.
	 * 
	 * @var array<string, array<string, TriggerInterface>>
	 */
	protected array $_triggers = [];
	
	/**
	 * The collation of the table.
	 *
	 * @var CollationInterface
	 */
	protected CollationInterface $_collation;
	
	/**
	 * The engine of the table.
	 *
	 * @var EngineInterface
	 */
	protected EngineInterface $_engine;
	
	/**
	 * The comment of the table.
	 *
	 * @var ?string
	 */
	protected ?string $_comment = null;
	
	/**
	 * Builds a new Table with the given name, collation, engine and comment.
	 * 
	 * @param string $name
	 * @param CollationInterface $collation
	 * @param EngineInterface $engine
	 * @param string $comment
	 */
	public function __construct(string $name, CollationInterface $collation, EngineInterface $engine, ?string $comment = null)
	{
		$this->_name = $name;
		$this->_collation = $collation;
		$this->_engine = $engine;
		$this->_comment = $comment;
		$this->_primaryKey = new PrimaryKey();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a column to this table.
	 *
	 * @param ColumnInterface $column
	 * @param boolean $addsToPrimaryKey
	 */
	public function addColumn(ColumnInterface $column, bool $addsToPrimaryKey = false) : void
	{
		$this->_columns[$column->getName()] = $column;
		if($addsToPrimaryKey)
		{
			$this->_primaryKey->addColumn($column->getName());
		}
	}
	
	/**
	 * Adds a foreign key to this table.
	 * 
	 * @param ForeignKeyInterface $foreignKey
	 */
	public function addForeignKey(ForeignKeyInterface $foreignKey) : void
	{
		$this->_foreignKeys[$foreignKey->getName()] = $foreignKey;
	}
	
	/**
	 * Adds an index to this table.
	 *
	 * @param IndexInterface $index
	 */
	public function addIndex(IndexInterface $index) : void
	{
		$this->_indexes[$index->getName()] = $index;
	}
	
	/**
	 * Sets the trigger for the given handler and event.
	 * 
	 * @param TriggerInterface $trigger
	 */
	public function setTrigger(TriggerInterface $trigger) : void
	{
		$this->_triggers[$trigger->getHandler()->getHandlerName()][$trigger->getEvent()->getEventName()] = $trigger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getColumns()
	 * @return Iterator<ColumnInterface>
	 */
	public function getColumns() : Iterator
	{
		return new ArrayIterator($this->_columns);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getPrimaryKey()
	 */
	public function getPrimaryKey() : PrimaryKeyInterface
	{
		return $this->_primaryKey;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getForeignKeys()
	 * @return Iterator<ForeignKeyInterface>
	 */
	public function getForeignKeys() : Iterator
	{
		return new ArrayIterator($this->_foreignKeys);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getIndexes()
	 * @return Iterator<IndexInterface>
	 */
	public function getIndexes() : Iterator
	{
		return new ArrayIterator($this->_indexes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getTriggers()
	 * @return Iterator<TriggerInterface>
	 */
	public function getTriggers() : Iterator
	{
		$triggers = [];
		
		foreach($this->_triggers as $triggerList)
		{
			foreach($triggerList as $trigger)
			{
				$triggers[] = $trigger;
			}
		}
		
		return new ArrayIterator($triggers);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getDefaultCollation()
	 */
	public function getDefaultCollation() : CollationInterface
	{
		return $this->_collation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getEngine()
	 */
	public function getEngine() : EngineInterface
	{
		return $this->_engine;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TableInterface::getComment()
	 */
	public function getComment() : ?string
	{
		return $this->_comment;
	}
	
}
