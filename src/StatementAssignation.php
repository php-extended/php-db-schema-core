<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementAssignation class file.
 * 
 * This class is a simple implementation of the StatementAssignationInterface.
 * 
 * @author Anastaszor
 */
class StatementAssignation implements StatementAssignationInterface
{
	
	/**
	 * The assignment operator.
	 * 
	 * @var string
	 */
	protected string $_operator;
	
	/**
	 * The variable that is assigned the value.
	 * 
	 * @var StatementAssignableInterface
	 */
	protected StatementAssignableInterface $_variable;
	
	/**
	 * The value of the assignation.
	 * 
	 * @var StatementValueInterface
	 */
	protected StatementValueInterface $_statement;
	
	/**
	 * Builds a new StatementAssignation with the given operator, variable and
	 * value.
	 * 
	 * @param string $operator
	 * @param StatementAssignableInterface $variable
	 * @param StatementValueInterface $statement
	 */
	public function __construct(string $operator, StatementAssignableInterface $variable, StatementValueInterface $statement)
	{
		$this->_operator = $operator;
		$this->_variable = $variable;
		$this->_statement = $statement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementAssignationInterface::getOperator()
	 * @return string
	 */
	public function getOperator() : string
	{
		return $this->_operator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementAssignationInterface::getVariable()
	 */
	public function getVariable() : StatementAssignableInterface
	{
		return $this->_variable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementAssignationInterface::getStatement()
	 */
	public function getStatement() : StatementValueInterface
	{
		return $this->_statement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitStatementAssignation($this);
	}
	
}
