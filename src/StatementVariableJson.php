<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableJson class file.
 * 
 * This class is a simple implementation of the StatementVariableJsonInterface.
 * 
 * @author Anastaszor
 */
class StatementVariableJson extends AbstractStatementVariable implements StatementVariableJsonInterface
{
	
	/**
	 * The type of variable.
	 * 
	 * @var TypeJsonInterface
	 */
	protected TypeJsonInterface $_type;
	
	/**
	 * Builds a new StatementVariableJson with the given name and type.
	 * 
	 * @param string $name
	 * @param TypeJsonInterface $type
	 */
	public function __construct(string $name, TypeJsonInterface $type)
	{
		parent::__construct($name);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueJsonInterface::getType()
	 */
	public function getType() : TypeJsonInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitVariableJson($this);
	}
	
}
