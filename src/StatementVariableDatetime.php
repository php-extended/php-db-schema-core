<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableDatetime class file.
 * 
 * This class is a simple implementation of the StatementVariableDatetimeInterface.
 * 
 * @author Anastaszor
 */
class StatementVariableDatetime extends AbstractStatementVariable implements StatementVariableDatetimeInterface
{
	
	/**
	 * The type of variable.
	 * 
	 * @var TypeDatetimeInterface
	 */
	protected TypeDatetimeInterface $_type;
	
	/**
	 * Builds a new StatementVariableDatetime with the given name and type.
	 * 
	 * @param string $name
	 * @param TypeDatetimeInterface $type
	 */
	public function __construct(string $name, TypeDatetimeInterface $type)
	{
		parent::__construct($name);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueDatetimeInterface::getType()
	 */
	public function getType() : TypeDatetimeInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitVariableDatetime($this);
	}
	
}
