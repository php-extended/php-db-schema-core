<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnCollection class file.
 *
 * This class is a base implementation of the ColumnCollectionInterface.
 *
 * @author Anastaszor
 */
class ColumnCollection extends AbstractColumn implements ColumnCollectionInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeCollectionInterface
	 */
	protected TypeCollectionInterface $_type;
	
	/**
	 * All the allowed values.
	 *
	 * @var array<integer, string>
	 */
	protected array $_values = [];
	
	/**
	 * Builds a new ColumnCollection with the given collection type.
	 * 
	 * @param string $name
	 * @param TypeCollectionInterface $type
	 * @param array<integer|string, boolean|integer|float|string> $values
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeCollectionInterface $type, array $values, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
		
		foreach($values as $value)
		{
			$this->addValue((string) $value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnCollection($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnCollectionInterface::getType()
	 */
	public function getType() : TypeCollectionInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnCollectionInterface::getValues()
	 */
	public function getValues() : array
	{
		return $this->_values;
	}
	
	/**
	 * Adds a specific value to the list of allowed values.
	 *
	 * @param string $value
	 */
	protected function addValue(string $value) : void
	{
		if(!\in_array($value, $this->_values, true))
		{
			$this->_values[] = $value;
		}
	}
	
}
