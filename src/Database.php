<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * Database class file.
 * 
 * This class is a simple implementation of the DatabaseInterface.
 * 
 * @author Anastaszor
 */
class Database implements DatabaseInterface
{
	
	/**
	 * The name of the database.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The default collation of the database.
	 * 
	 * @var CollationInterface
	 */
	protected CollationInterface $_defaultCollation;
	
	/**
	 * The schemas contained in this database.
	 * 
	 * @var array<string, SchemaInterface>
	 */
	protected array $_schemas = [];
	
	/**
	 * Builds a new Database with the given name.
	 * 
	 * @param string $name
	 * @param CollationInterface $defaultCollation
	 */
	public function __construct(string $name, CollationInterface $defaultCollation)
	{
		$this->_name = \trim($name);
		$this->_defaultCollation = $defaultCollation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DatabaseInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DatabaseInterface::getDefaultCollation()
	 */
	public function getDefaultCollation() : CollationInterface
	{
		return $this->_defaultCollation;
	}
	
	/**
	 * Adds a schema to this database.
	 * 
	 * @param SchemaInterface $schema
	 */
	public function addSchema(SchemaInterface $schema) : void
	{
		$this->_schemas[$schema->getName()] = $schema;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\DatabaseInterface::getSchemas()
	 * @return Iterator<SchemaInterface>
	 */
	public function getSchemas() : Iterator
	{
		return new ArrayIterator($this->_schemas);
	}
	
}
