<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnJson class file.
 *
 * This class is a base implementation of the ColumnJsonInterface.
 *
 * @author Anastaszor
 */
class ColumnJson extends AbstractColumn implements ColumnJsonInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeJsonInterface
	 */
	protected TypeJsonInterface $_type;
	
	/**
	 * Builds a new ColumnJson with the given json type.
	 * 
	 * @param string $name
	 * @param TypeJsonInterface $type
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeJsonInterface $type, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnJson($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\AbstractColumn::getType()
	 */
	public function getType() : TypeJsonInterface
	{
		return $this->_type;
	}
	
}
