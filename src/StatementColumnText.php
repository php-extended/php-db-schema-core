<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnText class file.
 * 
 * This class is a simple implementation of the StatementColumnTextInterface.
 * 
 * @author Anastaszor
 */
class StatementColumnText implements StatementColumnTextInterface
{
	
	/**
	 * The access modifier.
	 * 
	 * @var StatementAccessModifierInterface
	 */
	protected StatementAccessModifierInterface $_accessModifier;
	
	/**
	 * The datetime column.
	 * 
	 * @var ColumnTextInterface
	 */
	protected ColumnTextInterface $_column;
	
	/**
	 * Builds a new StatementColumnText with the given datetime
	 * column.
	 * 
	 * @param StatementAccessModifierInterface $modifier
	 * @param ColumnTextInterface $column
	 */
	public function __construct(StatementAccessModifierInterface $modifier, ColumnTextInterface $column)
	{
		$this->_accessModifier = $modifier;
		$this->_column = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnInterface::getAccessModifier()
	 */
	public function getAccessModifier() : StatementAccessModifierInterface
	{
		return $this->_accessModifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnTextInterface::getColumn()
	 */
	public function getColumn() : ColumnTextInterface
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueTextInterface::getType()
	 */
	public function getType() : TypeTextInterface
	{
		return $this->getColumn()->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitColumnText($this);
	}
	
}
