<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVisitor class file.
 * 
 * This class is a simple implemnetation of the StatementVisitorInterface that
 * does nothing.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyMethods")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class StatementVisitor implements StatementVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementAssignation()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementAssignation(StatementAssignationInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementComparison()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementComparison(StatementComparisonInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitStatementDeclaration()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStatementDeclaration(StatementDeclarationInterface $statement)
	{
		return null;
	}
	
	// ---- ---- ---- ---- Fixed Values Visits ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueDatetime()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueDatetime(StatementFixedValueDatetimeInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueJson()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueJson(StatementFixedValueJsonInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueNumber()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueNumber(StatementFixedValueNumberInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueSpatial()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueSpatial(StatementFixedValueSpatialInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueString()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueString(StatementFixedValueStringInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFixedValueText()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFixedValueText(StatementFixedValueTextInterface $statement)
	{
		return null;
	}
	
	
	// ---- ---- ---- ---- Variables Visits ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableDatetime()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableDatetime(StatementVariableDatetimeInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableJson()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableJson(StatementVariableJsonInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableNumber()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableNumber(StatementVariableNumberInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableSpatial()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableSpatial(StatementVariableSpatialInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableString()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableString(StatementVariableStringInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitVariableText()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitVariableText(StatementVariableTextInterface $statement)
	{
		return null;
	}
	
	
	// ---- ---- ---- ---- Function Calls Visits ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallDatetime()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallDatetime(StatementFunctionCallDatetimeInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallJson()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallJson(StatementFunctionCallJsonInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallNumber()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallNumber(StatementFunctionCallNumberInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallSpatial()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallSpatial(StatementFunctionCallSpatialInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallString()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallString(StatementFunctionCallStringInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitFunctionCallText()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFunctionCallText(StatementFunctionCallTextInterface $statement)
	{
		return null;
	}
	
	// ---- ---- ---- ---- Column Visits ---- ---- ---- ---- \\
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnDatetime()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnDatetime(StatementColumnDatetimeInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnJson()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnJson(StatementColumnJsonInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnNumber()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnNumber(StatementColumnNumberInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnSpatial()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnSpatial(StatementColumnSpatialInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnString()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnString(StatementColumnStringInterface $statement)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVisitorInterface::visitColumnText()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnText(StatementColumnTextInterface $statement)
	{
		return null;
	}
	
}
