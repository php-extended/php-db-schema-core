<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallNumber class file.
 * 
 * This class represents a simple implementation of the StatementFunctionCallNumberInterface.
 * 
 * @author Anastaszor
 */
class StatementFunctionCallNumber extends AbstractStatementFunctionCall implements StatementFunctionCallNumberInterface
{
	
	/**
	 * The return type of this function call.
	 * 
	 * @var TypeNumberInterface
	 */
	protected TypeNumberInterface $_type;
	
	/**
	 * Builds a new StatementFunctionCallNumber with its name, return type
	 * and argument types.
	 * 
	 * @param string $name
	 * @param TypeNumberInterface $return
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, TypeNumberInterface $return, array $parameters)
	{
		parent::__construct($name, $parameters);
		$this->_type = $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueNumberInterface::getType()
	 */
	public function getType() : TypeNumberInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFunctionCallNumber($this);
	}
	
}
