<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ForeignKeyRelation class file.
 * 
 * This class is a simple implementation of the ForeignKeyRelationInterface.
 * 
 * @author Anastaszor
 */
class ForeignKeyRelation implements ForeignKeyRelationInterface
{
	
	/**
	 * The name of the field in the source table.
	 * 
	 * @var string
	 */
	protected string $_sourceFieldName;
	
	/**
	 * The name of the field in the target table.
	 * 
	 * @var string
	 */
	protected string $_targetFieldName;
	
	/**
	 * Builds a new ForeignKeyRelation with the given source and target field
	 * names.
	 * 
	 * @param string $sourceFieldName
	 * @param string $targetFieldName
	 */
	public function __construct(string $sourceFieldName, string $targetFieldName)
	{
		$this->_sourceFieldName = \trim($sourceFieldName);
		$this->_targetFieldName = \trim($targetFieldName);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyRelationInterface::getSourceFieldName()
	 */
	public function getSourceFieldName() : string
	{
		return $this->_sourceFieldName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ForeignKeyRelationInterface::getTargetFieldName()
	 */
	public function getTargetFieldName() : string
	{
		return $this->_targetFieldName;
	}
	
}
