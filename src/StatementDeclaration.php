<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementDeclaration class file.
 * 
 * This class is a simple implementation of the StatementDeclarationInterface.
 * 
 * @author Anastaszor
 */
class StatementDeclaration implements StatementDeclarationInterface
{
	
	/**
	 * The variable.
	 * 
	 * @var StatementVariableInterface
	 */
	protected StatementVariableInterface $_variable;
	
	/**
	 * The type of the variable.
	 * 
	 * @var TypeInterface
	 */
	protected TypeInterface $_type;
	
	/**
	 * Builds a new StatementDeclaration with the given variable name and type.
	 * 
	 * @param StatementVariableInterface $variable
	 * @param TypeInterface $type
	 */
	public function __construct(StatementVariableInterface $variable, TypeInterface $type)
	{
		$this->_variable = $variable;
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementDeclarationInterface::getVariableName()
	 */
	public function getVariableName() : StatementVariableInterface
	{
		return $this->_variable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementDeclarationInterface::getVariableType()
	 */
	public function getVariableType() : TypeInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitStatementDeclaration($this);
	}
	
}
