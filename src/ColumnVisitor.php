<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnVisitor class file.
 * 
 * This class is a simple implementation of the ColumnVisitorInterface that
 * does nothing.
 * 
 * @author Anastaszor
 */
class ColumnVisitor implements ColumnVisitorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnCollection()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnCollection(ColumnCollectionInterface $columnCollection)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnDatetime()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnDatetime(ColumnDatetimeInterface $columnDatetime)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnJson()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnJson(ColumnJsonInterface $columnJson)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnNumber()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnNumber(ColumnNumberInterface $columnNumber)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnSpatial()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnSpatial(ColumnSpatialInterface $columnSpatial)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnString()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnString(ColumnStringInterface $columnString)
	{
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnVisitorInterface::visitColumnText()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitColumnText(ColumnTextInterface $columnText)
	{
		return null;
	}
	
}
