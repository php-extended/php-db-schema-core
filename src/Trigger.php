<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * Trigger class file.
 * 
 * This class is a simple implementation of the trigger interface.
 * 
 * @author Anastaszor
 */
class Trigger implements TriggerInterface
{
	
	/**
	 * The name of the trigger.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The trigger event.
	 * 
	 * @var TriggerEventInterface
	 */
	protected TriggerEventInterface $_event;
	
	/**
	 * The trigger handler.
	 * 
	 * @var TriggerHandlerInterface
	 */
	protected TriggerHandlerInterface $_handler;
	
	/**
	 * The trigger statements.
	 * 
	 * @var array<integer, StatementInterface>
	 */
	protected array $_statements = [];
	
	/**
	 * Builds a new Trigger with the given event, handler and statements.
	 * 
	 * @param string $name
	 * @param TriggerEventInterface $event
	 * @param TriggerHandlerInterface $handler
	 * @param array<integer|string, StatementInterface> $statements
	 */
	public function __construct(string $name, TriggerEventInterface $event, TriggerHandlerInterface $handler, array $statements = [])
	{
		$this->_name = $name;
		$this->_event = $event;
		$this->_handler = $handler;
		$this->_statements = \array_values($statements);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a statement to the list of the statements this trigger does.
	 * 
	 * @param StatementInterface $statement
	 */
	public function addStatement(StatementInterface $statement) : void
	{
		$this->_statements[] = $statement;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TriggerInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TriggerInterface::getEvent()
	 */
	public function getEvent() : TriggerEventInterface
	{
		return $this->_event;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TriggerInterface::getHandler()
	 */
	public function getHandler() : TriggerHandlerInterface
	{
		return $this->_handler;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\TriggerInterface::getStatements()
	 * @return Iterator<StatementInterface>
	 */
	public function getStatements() : Iterator
	{
		return new ArrayIterator($this->_statements);
	}
	
}
