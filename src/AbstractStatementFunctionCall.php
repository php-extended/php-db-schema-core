<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * AbstractStatementFunctionCall interface file.
 * 
 * This class is a simple implementation of the StatementFunctionCallInterface.
 * 
 * @author Anastaszor
 */
abstract class AbstractStatementFunctionCall implements StatementFunctionCallInterface
{
	
	/**
	 * The name of the function call.
	 * 
	 * @var string
	 */
	protected string $_functionName;
	
	/**
	 * The parameters of the function call.
	 * 
	 * @var array<integer, StatementValueInterface>
	 */
	protected array $_parameters = [];
	
	/**
	 * Builds a new StatementFunctionCall with the given name and parameters.
	 * 
	 * @param string $name
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, array $parameters)
	{
		$this->_functionName = \trim($name);
		$this->_parameters = \array_values($parameters);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementFunctionCallInterface::getFunctionName()
	 */
	public function getFunctionName() : string
	{
		return $this->_functionName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementFunctionCallInterface::getParameters()
	 * @return Iterator<StatementValueInterface>
	 */
	public function getParameters() : Iterator
	{
		return new ArrayIterator($this->_parameters);
	}
	
}
