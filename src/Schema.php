<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

use ArrayIterator;
use Iterator;

/**
 * Schema class file.
 * 
 * This class is a simple implementation of the SchemaInterface.
 * 
 * @author Anastaszor
 */
class Schema implements SchemaInterface
{
	
	/**
	 * The name of this schema.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The default collation of the schema.
	 * 
	 * @var CollationInterface
	 */
	protected CollationInterface $_defaultCollation;
	
	/**
	 * The tables of this schema.
	 * 
	 * @var array<string, TableInterface>
	 */
	protected array $_tables = [];
	
	/**
	 * Builds a new Schema with the given name.
	 * 
	 * @param string $name
	 * @param CollationInterface $defaultCollation
	 */
	public function __construct(string $name, CollationInterface $defaultCollation)
	{
		$this->_name = $name;
		$this->_defaultCollation = $defaultCollation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\SchemaInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\SchemaInterface::getDefaultCollation()
	 */
	public function getDefaultCollation() : CollationInterface
	{
		return $this->_defaultCollation;
	}
	
	/**
	 * Adds a table to the given schema.
	 * 
	 * @param TableInterface $table
	 */
	public function addTable(TableInterface $table) : void
	{
		$this->_tables[$table->getName()] = $table;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\SchemaInterface::getTables()
	 * @return Iterator<TableInterface>
	 */
	public function getTables() : Iterator
	{
		return new ArrayIterator($this->_tables);
	}
	
}
