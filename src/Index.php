<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * Index class file.
 *
 * This class is a base implementation of the IndexInterface.
 *
 * @author Anastaszor
 */
class Index implements IndexInterface
{
	
	/**
	 * The name of the index.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Whether this index has an unique constraint.
	 *
	 * @var boolean
	 */
	protected bool $_isUnique = false;
	
	/**
	 * The names of the columns of this index.
	 *
	 * @var array<integer, string>
	 */
	protected array $_columnNames = [];
	
	/**
	 * Builds a new Index with its name and the columns it's applied to.
	 * 
	 * @param string $name
	 * @param boolean $unique
	 * @param array<integer|string, ColumnInterface> $columnNames
	 */
	public function __construct(string $name, bool $unique, array $columnNames)
	{
		$this->_name = \trim($name);
		$this->_isUnique = $unique;
		$this->_columnNames = [];
		
		foreach($columnNames as $column)
		{
			$columnName = \trim($column->getName());
			if(empty($columnName))
			{
				continue;
			}
			if(\in_array($columnName, $this->_columnNames, true))
			{
				continue;
			}
			$this->_columnNames[] = $columnName;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds a column to this primary key.
	 *
	 * @param string $column
	 */
	public function addColumn(string $column) : void
	{
		$this->_columnNames[] = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\IndexInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\IndexInterface::isUnique()
	 */
	public function isUnique() : bool
	{
		return $this->_isUnique;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\IndexInterface::getColumnNames()
	 */
	public function getColumnNames() : array
	{
		return $this->_columnNames;
	}
	
}
