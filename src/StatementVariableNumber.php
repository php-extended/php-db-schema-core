<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableNumber class file.
 * 
 * This class is a simple implementation of the StatementVariableNumberInterface.
 * 
 * @author Anastaszor
 */
class StatementVariableNumber extends AbstractStatementVariable implements StatementVariableNumberInterface
{
	
	/**
	 * The type of variable.
	 * 
	 * @var TypeNumberInterface
	 */
	protected TypeNumberInterface $_type;
	
	/**
	 * Builds a new StatementVariableNumber with the given name and type.
	 * 
	 * @param string $name
	 * @param TypeNumberInterface $type
	 */
	public function __construct(string $name, TypeNumberInterface $type)
	{
		parent::__construct($name);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueNumberInterface::getType()
	 */
	public function getType() : TypeNumberInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitVariableNumber($this);
	}
	
}
