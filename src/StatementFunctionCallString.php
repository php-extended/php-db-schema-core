<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementFunctionCallString class file.
 * 
 * This class represents a simple implementation of the StatementFunctionCallStringInterface.
 * 
 * @author Anastaszor
 */
class StatementFunctionCallString extends AbstractStatementFunctionCall implements StatementFunctionCallStringInterface
{
	
	/**
	 * The return type of this function type.
	 * 
	 * @var TypeStringInterface
	 */
	protected TypeStringInterface $_type;
	
	/**
	 * Builds a new StatementFunctionCallString with its name, return type
	 * and argument types.
	 * 
	 * @param string $name
	 * @param TypeStringInterface $return
	 * @param array<integer|string, StatementValueInterface> $parameters
	 */
	public function __construct(string $name, TypeStringInterface $return, array $parameters)
	{
		parent::__construct($name, $parameters);
		$this->_type = $return;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueStringInterface::getType()
	 */
	public function getType() : TypeStringInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitFunctionCallString($this);
	}
	
}
