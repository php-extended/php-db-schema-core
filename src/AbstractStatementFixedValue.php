<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * AbstractStatementFixedValue class file.
 * 
 * This class is a simple implementation of the StatementFixedValueInterface.
 * 
 * @author Anastaszor
 */
abstract class AbstractStatementFixedValue implements StatementFixedValueInterface
{
	
	/**
	 * The value.
	 * 
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new AbstractStatementFixedValue with the value and its type.
	 * 
	 * @param string $value
	 */
	public function __construct(string $value)
	{
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementFixedValueInterface::getFixedValue()
	 */
	public function getFixedValue() : string
	{
		return $this->_value;
	}
	
}
