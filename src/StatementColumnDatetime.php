<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnDatetime class file.
 * 
 * This class is a simple implementation of the StatementColumnDatetimeInterface.
 * 
 * @author Anastaszor
 */
class StatementColumnDatetime implements StatementColumnDatetimeInterface
{
	
	/**
	 * The access modifier.
	 * 
	 * @var StatementAccessModifierInterface
	 */
	protected StatementAccessModifierInterface $_accessModifier;
	
	/**
	 * The datetime column.
	 * 
	 * @var ColumnDatetimeInterface
	 */
	protected ColumnDatetimeInterface $_column;
	
	/**
	 * Builds a new StatementColumnDatetime with the given datetime
	 * column.
	 * 
	 * @param StatementAccessModifierInterface $modifier
	 * @param ColumnDatetimeInterface $column
	 */
	public function __construct(StatementAccessModifierInterface $modifier, ColumnDatetimeInterface $column)
	{
		$this->_accessModifier = $modifier;
		$this->_column = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnInterface::getAccessModifier()
	 */
	public function getAccessModifier() : StatementAccessModifierInterface
	{
		return $this->_accessModifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnDatetimeInterface::getColumn()
	 */
	public function getColumn() : ColumnDatetimeInterface
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueDatetimeInterface::getType()
	 */
	public function getType() : TypeDatetimeInterface
	{
		return $this->getColumn()->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitColumnDatetime($this);
	}
	
}
