<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * AbstractStatementComparison class file.
 * 
 * This class is a simple implementation of the StatementComparisonInterface.
 * 
 * @author Anastaszor
 */
abstract class AbstractStatementComparison implements StatementComparisonInterface, StatementValueNumberInterface
{
	
	/**
	 * The operator of the comparison.
	 * 
	 * @var string
	 */
	protected string $_operator;
	
	/**
	 * The left operand.
	 * 
	 * @var StatementValueInterface
	 */
	protected StatementValueInterface $_leftOperand;
	
	/**
	 * The right operand.
	 * 
	 * @var StatementValueInterface
	 */
	protected StatementValueInterface $_rightOperand;
	
	/**
	 * Builds a new AbstractStatementComparison with the given operator and operands.
	 * 
	 * @param string $operator
	 * @param StatementValueInterface $left
	 * @param StatementValueInterface $right
	 */
	public function __construct(string $operator, StatementValueInterface $left, StatementValueInterface $right)
	{
		$this->_operator = \trim($operator);
		$this->_leftOperand = $left;
		$this->_rightOperand = $right;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementComparisonInterface::getOperator()
	 */
	public function getOperator() : string
	{
		return $this->_operator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementComparisonInterface::getLeftOperand()
	 */
	public function getLeftOperand() : StatementValueInterface
	{
		return $this->_leftOperand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementComparisonInterface::getRightOperand()
	 */
	public function getRightOperand() : StatementValueInterface
	{
		return $this->_rightOperand;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitStatementComparison($this);
	}
	
}
