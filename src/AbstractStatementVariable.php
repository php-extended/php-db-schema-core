<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * AbstractStatementVariable class file.
 * 
 * This class is a simple implementation of the StatementVariableInterface.
 * 
 * @author Anastaszor
 */
abstract class AbstractStatementVariable implements StatementVariableInterface
{
	
	/**
	 * The name of the variable.
	 * 
	 * @var string
	 */
	protected string $_variableName;
	
	/**
	 * Builds a new AbstractStatementVariable with its name.
	 * 
	 * @param string $name
	 */
	public function __construct(string $name)
	{
		$this->_variableName = \trim($name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementVariableInterface::getVariableName()
	 */
	public function getVariableName() : string
	{
		return $this->_variableName;
	}
	
}
