<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnSpatial class file.
 *
 * This class is a base implementation of the ColumnSpatialInterface.
 *
 * @author Anastaszor
 */
class ColumnSpatial extends AbstractColumn implements ColumnSpatialInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeSpatialInterface
	 */
	protected TypeSpatialInterface $_type;
	
	/**
	 * Builds a new ColumnSpatial with the given spatial type.
	 * 
	 * @param string $name
	 * @param TypeSpatialInterface $type
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeSpatialInterface $type, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnSpatial($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnSpatialInterface::getType()
	 */
	public function getType() : TypeSpatialInterface
	{
		return $this->_type;
	}
	
}
