<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementVariableString class file.
 * 
 * This class is a simple implementation of this StatementVariableStringInterface.
 * 
 * @author Anastaszor
 */
class StatementVariableString extends AbstractStatementVariable implements StatementVariableStringInterface
{
	
	/**
	 * The type of variable.
	 * 
	 * @var TypeStringInterface
	 */
	protected TypeStringInterface $_type;
	
	/**
	 * Builds a new StatementVariableString with the given name and type.
	 * 
	 * @param string $name
	 * @param TypeStringInterface $type
	 */
	public function __construct(string $name, TypeStringInterface $type)
	{
		parent::__construct($name);
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueStringInterface::getType()
	 */
	public function getType() : TypeStringInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitVariableString($this);
	}
	
}
