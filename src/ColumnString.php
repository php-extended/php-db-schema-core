<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnString class file.
 *
 * This class is a base implementation of the ColumnStringInterface.
 *
 * @author Anastaszor
 */
class ColumnString extends AbstractColumn implements ColumnStringInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeStringInterface
	 */
	protected TypeStringInterface $_type;
	
	/**
	 * The choosen length for the type.
	 *
	 * @var integer
	 */
	protected int $_length;
	
	/**
	 * The choosen collation for the type.
	 *
	 * @var CollationInterface
	 */
	protected CollationInterface $_collation;
	
	/**
	 * Builds a new ColumnString with the given string type.
	 * 
	 * @param string $name
	 * @param TypeStringInterface $type
	 * @param integer $length
	 * @param CollationInterface $collation
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeStringInterface $type, int $length, CollationInterface $collation, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
		$this->_length = $length;
		$this->_collation = $collation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnString($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnStringInterface::getType()
	 */
	public function getType() : TypeStringInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnStringInterface::getLength()
	 */
	public function getLength() : int
	{
		return $this->_length;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnStringInterface::getCollation()
	 */
	public function getCollation() : CollationInterface
	{
		return $this->_collation;
	}
	
}
