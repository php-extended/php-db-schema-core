<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnJson class file.
 * 
 * This class is a simple implementation of the StatementColumnJsonInterface.
 * 
 * @author Anastaszor
 */
class StatementColumnJson implements StatementColumnJsonInterface
{
	
	/**
	 * The access modifier.
	 * 
	 * @var StatementAccessModifierInterface
	 */
	protected StatementAccessModifierInterface $_accessModifier;
	
	/**
	 * The json column.
	 * 
	 * @var ColumnJsonInterface
	 */
	protected ColumnJsonInterface $_column;
	
	/**
	 * Builds a new StatementColumnJson with the given json column.
	 * 
	 * @param StatementAccessModifierInterface $modifier
	 * @param ColumnJsonInterface $column
	 */
	public function __construct(StatementAccessModifierInterface $modifier, ColumnJsonInterface $column)
	{
		$this->_accessModifier = $modifier;
		$this->_column = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnInterface::getAccessModifier()
	 */
	public function getAccessModifier() : StatementAccessModifierInterface
	{
		return $this->_accessModifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnJsonInterface::getColumn()
	 */
	public function getColumn() : ColumnJsonInterface
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueJsonInterface::getType()
	 */
	public function getType() : TypeJsonInterface
	{
		return $this->getColumn()->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitColumnJson($this);
	}
	
}
