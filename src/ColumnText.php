<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * ColumnText class file.
 *
 * This class is a base implementation of the ColumnTextInterface.
 *
 * @author Anastaszor
 */
class ColumnText extends AbstractColumn implements ColumnTextInterface
{
	
	/**
	 * The type of column.
	 * 
	 * @var TypeTextInterface
	 */
	protected TypeTextInterface $_type;
	
	/**
	 * The choosen collation for the type.
	 *
	 * @var CollationInterface
	 */
	protected CollationInterface $_collation;
	
	/**
	 * Builds a new ColumnText with the given text type.
	 * 
	 * @param string $name
	 * @param TypeTextInterface $type
	 * @param CollationInterface $collation
	 * @param boolean $allowsNull
	 * @param string $comment
	 */
	public function __construct(string $name, TypeTextInterface $type, CollationInterface $collation, bool $allowsNull = false, ?string $comment = null)
	{
		parent::__construct($name, $allowsNull, $comment);
		$this->_type = $type;
		$this->_collation = $collation;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(ColumnVisitorInterface $visitor)
	{
		return $visitor->visitColumnText($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnTextInterface::getType()
	 */
	public function getType() : TypeTextInterface
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\ColumnTextInterface::getCollation()
	 */
	public function getCollation() : CollationInterface
	{
		return $this->_collation;
	}
	
}
