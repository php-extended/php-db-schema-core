<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DbSchema;

/**
 * StatementColumnSpatial class file.
 * 
 * This class is a simple implementation of the StatementColumnSpatialInterface.
 * 
 * @author Anastaszor
 */
class StatementColumnSpatial implements StatementColumnSpatialInterface
{
	
	/**
	 * The access modifier.
	 * 
	 * @var StatementAccessModifierInterface
	 */
	protected StatementAccessModifierInterface $_accessModifier;
	
	/**
	 * The datetime column.
	 * 
	 * @var ColumnSpatialInterface
	 */
	protected ColumnSpatialInterface $_column;
	
	/**
	 * Builds a new StatementColumnSpatial with the given datetime
	 * column.
	 * 
	 * @param StatementAccessModifierInterface $modifier
	 * @param ColumnSpatialInterface $column
	 */
	public function __construct(StatementAccessModifierInterface $modifier, ColumnSpatialInterface $column)
	{
		$this->_accessModifier = $modifier;
		$this->_column = $column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnInterface::getAccessModifier()
	 */
	public function getAccessModifier() : StatementAccessModifierInterface
	{
		return $this->_accessModifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementColumnSpatialInterface::getColumn()
	 */
	public function getColumn() : ColumnSpatialInterface
	{
		return $this->_column;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementValueSpatialInterface::getType()
	 */
	public function getType() : TypeSpatialInterface
	{
		return $this->getColumn()->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DbSchema\StatementInterface::beVisitedBy()
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(StatementVisitorInterface $visitor)
	{
		return $visitor->visitColumnSpatial($this);
	}
	
}
