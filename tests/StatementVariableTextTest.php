<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementVariableText;
use PhpExtended\DbSchema\TypeTextInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementVariableTextTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementVariableText
 *
 * @internal
 *
 * @small
 */
class StatementVariableTextTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementVariableText
	 */
	protected StatementVariableText $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementVariableText('name', $this->getMockForAbstractClass(TypeTextInterface::class));
	}
	
}
