<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementAssignableInterface;
use PhpExtended\DbSchema\StatementAssignation;
use PhpExtended\DbSchema\StatementValueInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementAssignationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementAssignation
 *
 * @internal
 *
 * @small
 */
class StatementAssignationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementAssignation
	 */
	protected StatementAssignation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementAssignation(
			'operator',
			$this->getMockForAbstractClass(StatementAssignableInterface::class),
			$this->getMockForAbstractClass(StatementValueInterface::class),
		);
	}
	
}
