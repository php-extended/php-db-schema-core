<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\CollationInterface;
use PhpExtended\DbSchema\EngineInterface;
use PhpExtended\DbSchema\Table;
use PHPUnit\Framework\TestCase;

/**
 * TableTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\Table
 *
 * @internal
 *
 * @small
 */
class TableTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Table
	 */
	protected Table $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Table(
			'name',
			$this->getMockForAbstractClass(CollationInterface::class),
			$this->getMockForAbstractClass(EngineInterface::class),
		);
	}
	
}
