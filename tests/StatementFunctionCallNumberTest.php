<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementFunctionCallNumber;
use PhpExtended\DbSchema\TypeNumberInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementFunctionCallNumberTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementFunctionCallNumber
 *
 * @internal
 *
 * @small
 */
class StatementFunctionCallNumberTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementFunctionCallNumber
	 */
	protected StatementFunctionCallNumber $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementFunctionCallNumber('name', $this->getMockForAbstractClass(TypeNumberInterface::class), []);
	}
	
}
