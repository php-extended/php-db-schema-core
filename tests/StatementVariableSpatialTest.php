<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementVariableSpatial;
use PhpExtended\DbSchema\TypeSpatialInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementVariableSpatialTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementVariableSpatial
 *
 * @internal
 *
 * @small
 */
class StatementVariableSpatialTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementVariableSpatial
	 */
	protected StatementVariableSpatial $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementVariableSpatial('name', $this->getMockForAbstractClass(TypeSpatialInterface::class));
	}
	
}
