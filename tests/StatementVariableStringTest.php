<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementVariableString;
use PhpExtended\DbSchema\TypeStringInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementVariableStringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementVariableString
 *
 * @internal
 *
 * @small
 */
class StatementVariableStringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementVariableString
	 */
	protected StatementVariableString $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementVariableString('name', $this->getMockForAbstractClass(TypeStringInterface::class));
	}
	
}
