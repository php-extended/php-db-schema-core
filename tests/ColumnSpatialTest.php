<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\ColumnSpatial;
use PhpExtended\DbSchema\TypeSpatialInterface;
use PHPUnit\Framework\TestCase;

/**
 * ColumnSpatialTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\ColumnSpatial
 *
 * @internal
 *
 * @small
 */
class ColumnSpatialTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ColumnSpatial
	 */
	protected ColumnSpatial $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ColumnSpatial('name', $this->getMockForAbstractClass(TypeSpatialInterface::class));
	}
	
}
