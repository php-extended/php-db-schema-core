<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\ColumnDatetimeInterface;
use PhpExtended\DbSchema\StatementAccessModifierInterface;
use PhpExtended\DbSchema\StatementColumnDatetime;
use PHPUnit\Framework\TestCase;

/**
 * StatementColumnDatetimeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementColumnDatetime
 *
 * @internal
 *
 * @small
 */
class StatementColumnDatetimeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementColumnDatetime
	 */
	protected StatementColumnDatetime $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementColumnDatetime(
			$this->getMockForAbstractClass(StatementAccessModifierInterface::class),
			$this->getMockForAbstractClass(ColumnDatetimeInterface::class),
		);
	}
	
}
