<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-db-schema-core library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DbSchema\StatementFixedValueSpatial;
use PhpExtended\DbSchema\TypeSpatialInterface;
use PHPUnit\Framework\TestCase;

/**
 * StatementFixedValueSpatialTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DbSchema\StatementFixedValueSpatial
 *
 * @internal
 *
 * @small
 */
class StatementFixedValueSpatialTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var StatementFixedValueSpatial
	 */
	protected StatementFixedValueSpatial $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new StatementFixedValueSpatial('value', $this->getMockForAbstractClass(TypeSpatialInterface::class));
	}
	
}
